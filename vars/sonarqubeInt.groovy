def call(String server) {
    
    stage('SonarQube Analysis') {
        def scannerHome = tool 'SonarScanner';
            withSonarQubeEnv(server) {
                sh "${scannerHome}/bin/sonar-scanner"
            }

    }
    stage("Quality Gate"){
        timeout(time: 10, unit: 'MINUTES') {
            def qg = waitForQualityGate() 
            if (qg.status != 'OK') {
               error "Pipeline aborted due to quality gate failure: ${qg.status}"
            }
            // echo "skipped qiality gate"
        }
    }
}
